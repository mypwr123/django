from django.urls import path

from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

# Lista adresów URL
urlpatterns = [
    # Strona główna
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('admin/', admin.site.urls),    
    # Strona rejestracji
    path('sign-up/', views.sign_up, name='sign_up'),
    path('sign-up-lekarz/', views.sign_up_lekarz, name='sign_up_lekarz'),
    
    # wizyty itp
    
    path('create_wizyta/', views.createWizyta, name='create_wizyta'),
    path('update_wizyta/<int:pk>/', views.updateWizyta, name='update_wizyta'),
    path('delete_wizyta/<int:pk>/', views.deleteWizyta, name='delete_wizyta'),


    path('profil/<int:pk_idprofil>/', views.profil, name='profil'),
    path('lprofil/<int:pk_idlprofil/', views.lprofil, name='lprofil'),
    #djangowe resetowanie hasła
    path('reset_password/', auth_views.PasswordResetView.as_view(), name="reset_password"),                     
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(), name="password_reset_done"),
    path('reset/<uid64>/<token>', auth_views.PasswordResetConfirmView.as_view(),name="password_reset_confirm"),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(),name="password_reset_complete"),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



