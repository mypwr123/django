from django.http import HttpResponse
from django.shortcuts import redirect

def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwags):
        if request.user.is_unauthenticated:
            return redirect('home')
        else:
            return view_func(request, *args, **kwags)
        
    return wrapper_func

def allowed_user(allowed_roles=[]):         #sprawdza czy zalogowany uzytkownik jest w odpowiedniej grupie 
    def decorator(view_func):
        def wrapper_func(request, *args, **kwags):
            group=None
            if request.user.group.exists():
                group = request.user.group.all()[0].name
            if group in allowed_roles:
                return view_func(request, *args, **kwags)
            else:
                return redirect(redirect_here)
        return wrapper_func
    return decorator



def admin_only(view_func):
    def wrapper_function(request, *args, **kwags):
        group = None
        if request.user.groups.exist():
            group = request.uesr.groups.all()[0].name

        if group=='lekarz' or 'pacjent' or 'default':
            return redirect('user-page')
        
        if group == 'admin':
            return view_func(request, *args, **kwags)
    
    return wrapper_function


def profil_access(view_func):
    def wrapper_function(request, *args, **kwags):
        group = None
        if request.user.groups.exist():
            group = request.user.groups.all()[0].name
        
        if request.user.id == request.profile.user:                         #czy profil jest zalogowanego usera
            return wrapper_function
        
        for wizyta in wizyta:                                          #czy zalogowany user jest jako lekarz w wizycie z userem profilu
            if request.user.id == request.wizyta.lekarz and request.profil.id ==  request.wizyta.pacjent:   
               return wrapper_function 







