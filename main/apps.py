from django.apps import AppConfig
from django.conf import settings

# Konfiguracja aplikacji "main"
class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'

    # Funkcja "ready" wywoływana podczas uruchamiania aplikacji
    def ready(self):
        from django.contrib.auth.models import Group
        from django.db.models.signals import post_save

        # Funkcja "add_to_default_group" dodaje użytkownika do grupy "default"
        def add_to_default_group(sender, **kwargs):
            user = kwargs["instance"]
            if kwargs['created']:
                group, ok = Group.objects.get_or_create(name="default")
                group.user_set.add(user)

        # Rejestrowanie sygnału "post_save" dla modelu użytkownika
        post_save.connect(add_to_default_group,
                          sender=settings.AUTH_USER_MODEL)
