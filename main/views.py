from django.shortcuts import render, redirect
from .forms import RegisterForm, RegisterFormLekarz
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User, Group
from .models import  Profil, LProfil ,Wizyta
from .decorators import unauthenticated_user, allowed_user
from .forms import WizytaForm



# Widok strony głównej, wymaga zalogowania użytkownika
#@login_required(login_url="/login")            #to sprawdza i przepuszcza jeśli jest zalogowany a jak nie to otwiera logowanie 
def home(request):
    return render(request, 'main/home.html')

    
# Widok rejestracji nowego użytkownika
def sign_up(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            # Utworzenie nowego użytkownika na podstawie danych z formularza
            user = form.save()
            group = Group.objects.get(name='pacjent')
            user.groups.add(group)
            # Logowanie użytkownika
            login(request, user)
            return redirect('/home')
    else:
        form = RegisterForm()

    return render(request, 'registration/sign_up.html', {"form": form})



# Widok rejestracji nowego użytkownika jako lekarz
def sign_up_lekarz(request):
    if request.method == 'POST':
        form = RegisterFormLekarz(request.POST)
        if form.is_valid():
            # Utworzenie nowego użytkownika na podstawie danych z formularza
            user = form.save()
            group = Group.objects.get(name='lekarze')
            user.groups.add(group)
            # Logowanie użytkownika
            login(request, user)
            return redirect('/home')
    else:
        form = RegisterFormLekarz()

    return render(request, 'registration/sign_up.html', {"form": form})


@login_required()
#@allowed_user()
def profil(request, pk_idprofil):
    profil = Profil.objects.get(id=pk_idprofil)
    context={'profil':profil}
    return render(request, 'profil/profil.html', context)


@login_required()
#@allowed_user()
def lprofil(request, pk_idlprofil):
    lprofil = Profil.objects.get(id=pk_idlprofil)
    context={'lprofil':profil}
    return render(request, 'profil/lprofil.html', context)


@login_required()
#@allowed_user()
def is_lekarze(request):
    is_lekarze = request.user.groups.filter(name='lekarze').exists()
    context = {'is_lekarze': is_lekarze}
    return render(request, 'nazwa_szablonu.html', context)



#@login_required(login_url="/login")
def createWizyta(request):
    
    form =WizytaForm()
    if request.method == 'POST':
        form =WizytaForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('/')            #pozniej dodac wizyty 
    context={'form':form}
    return render(request, 'wizyta/create_wizyta.html', context)

def updateWizyta(request, pk):
    
    wizyta = Wizyta.objects.get(id=pk)
    form = WizytaForm(instance=wizyta)

    if request.method == 'POST':
        form =WizytaForm(request.POST, instance=wizyta)
    if form.is_valid():
        form.save()
        return redirect('/')            #pozniej dodac wizyty 

    context={'form':form}
    return render(request, 'wizyta/update_wizyta.html', context)

def deleteWizyta(request,pk):
    wizyta = Wizyta.objects.get(id=pk)
    context={'wizytaa':wizyta}
    return render(request,'wizyta/delete.html', context)


    