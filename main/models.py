from django.db import models
from django.contrib.auth.models import User



class Profil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profil_pics')
    full_name = models.CharField(blank=True, null=True, max_length=100)
    date_of_birth = models.DateField(blank=True, null=True)
    addres = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=9, blank=True)
    bio = models.TextField(blank=True, null=True)
    insurance = models.TextField(blank=True, null=True, max_length=100)


    def __str__(self):
        return f'{self.user.username} Profil'
    
    
class Tag(models.Model):
    name= models.CharField(max_length=20)    



class LProfil(models.Model):
    TIME=('yes','yes'),('no','no')

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='lprofil_pics')
    tags= models.ManyToManyField(Tag)
    addres = models.CharField(max_length=100)
    bio = models.TextField(blank=True, null=True)
    phone_number = models.CharField(max_length=9, blank=True)
    company_name = models.CharField(blank=True, null=True,max_length=100)
    num_stars = models.IntegerField(blank=True, null=True)
    medical_degree = models.CharField(blank=True, null=True,max_length=100)
    available_time =models.CharField(choices=TIME ,max_length=5, default='yes')

    def __str__(self):
        return f'{self.user.username} LProfil'
    

    # Model wizyta
class Wizyta(models.Model):
    STATUS =[('planned','planned'),('canceled','canceled'),('done','done'),('confirmed','confirmed')]
    TIME=('yes','yes'),('no','no')
    # Pole author przechowuje informację o autorze postu
    lekarz=models.ForeignKey(LProfil, null=True,on_delete=models.CASCADE)     
    pacjent= models.ForeignKey(Profil, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    addres=models.CharField(max_length=200)                    
    description = models.TextField(blank=True, null=True)   
    status= models.CharField(max_length=20,choices=STATUS)                  
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    date_of_appointment = models.CharField(choices=TIME, default='yes',max_length=5)
    time_of_appointment = models.CharField(choices=TIME, default='yes',max_length=5)


    def __str__(self):
        # Metoda __str__ zwraca reprezentację tekstową postu (tytuł + opis)
        return self.title + "\n" + self.description + "\n" + str(self.pacjent) #+ "\n" + self.lekarz #to trzeba zmienić
    

    
class WizytaPreset(models.Model):
    lekarz = models.ForeignKey(LProfil, null=True,on_delete=models.CASCADE)       
    title = models.CharField(max_length=200)
    addres = models.CharField(max_length=200)                    
    description = models.TextField(blank=True, null=True)                    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    time_needed = models.IntegerField(blank=True, null=True)

    def __str__(self):
        
        return self.title + "\n" + self.description + "\n" + str(self.pacjent) #+ "\n" + self.lekarz #to trzeba zmienić