from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import  Profil, LProfil, Wizyta
from django.forms import ModelForm


class RegisterForm(UserCreationForm): #tu moge dodawać pola w rejestracji
    email = forms.EmailField(required=True)
    
    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]

    def save(self, commit=True):
        user = super().save(commit=False)
        profil = Profil()
        if commit:
            user.save()  # Zapisanie użytkownika
            profil.user = user  # Powiązanie profilu z użytkownikiem
            profil.save()  # Zapisanie profilu użytkownika
        return user


class RegisterFormLekarz(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]

    def save(self, commit=True):
        user = super().save(commit=False)
        Lprofil = LProfil()
        if commit:
            user.save()  # Zapisanie użytkownika
            Lprofil.user = user  # Powiązanie profilu z użytkownikiem
            Lprofil.save()  # Zapisanie profilu użytkownika
        return user



class WizytaForm(ModelForm):
    class Meta:
        model = Wizyta
        fields = '__all__'     
        
  
