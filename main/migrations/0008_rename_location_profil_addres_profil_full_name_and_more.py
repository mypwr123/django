# Generated by Django 4.2.2 on 2023-06-21 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_wizyta_lekarz_alter_lprofil_image_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profil',
            old_name='location',
            new_name='addres',
        ),
        migrations.AddField(
            model_name='profil',
            name='full_name',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='profil',
            name='insurance',
            field=models.TextField(blank=True, null=True),
        ),
    ]
