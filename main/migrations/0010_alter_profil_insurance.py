# Generated by Django 4.2.2 on 2023-06-21 15:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_alter_profil_full_name_alter_profil_insurance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profil',
            name='insurance',
            field=models.TextField(blank=True, max_length=100, null=True),
        ),
    ]
